'''
Created on 12 June 2558

@author: Sarucha Yanyong
'''


def parseCSV2Array(filename):
    '''
    @note: The first line will be skipped.
    @var filename: filename in string format.
    @return: Array of CSV
    '''
    output=[]
    f = open(filename);
    ix = 0;
    for line in f:
        if(ix!=0):
            line=line.strip();
            output.append(line.split("\t"))
        ix+=1;
    return output;
